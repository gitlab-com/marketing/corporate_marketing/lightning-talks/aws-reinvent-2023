# AWS Re:Invent 2023 Lightning Talks

This project contains the lightning talks presented at the GitLab booth during [AWS Re:Invent 2023](https://reinvent.awsevents.com/). This event took place November 27 – December 1, 2023 in Las Vegas, NV, USA.

GitLab is the most comprehensive AI-powered DevSecOps platform. Learn [why enterprises choose GitLab](https://about.gitlab.com/why-gitlab/)!

## Lightning Talks

| **Title** | **Description** | **Author** | **Video** |
| --------- | --------------- | ---------- | --------- |
| [TODO](./pdfs/) | N/A | N/A | N/A |
| [TODO](./pdfs/) | N/A | N/A | N/A |
